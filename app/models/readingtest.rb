class Readingtest < ActiveRecord::Base
  has_many :readingtest_questions, dependent: :destroy
  has_many :user_readingtests, dependent: :destroy

  def to_s
    name
  end
end
