class Vote < ActiveRecord::Base
  belongs_to :question
  has_many :vote_users, dependent: :destroy
  has_many :users, through: :vote_users, dependent: :destroy
end
