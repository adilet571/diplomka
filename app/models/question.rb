class Question < ActiveRecord::Base
  belongs_to :user
  belongs_to :forum
  has_and_belongs_to_many :tags
  has_many :answers, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_one :vote, dependent: :destroy


  def all_tags=(names)
    self.tags = names.split(",").map do |name|
      Tag.where(name: name.strip).first_or_create!
    end
  end

  def all_tags
    t = ""
    self.tags.each do |tag|
      t += tag.name+","
    end
  end

  def self.tagged_with(name)
    Tag.find_by_name!(name).questions
  end
end
