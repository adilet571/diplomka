class GrammartestQuestion < ActiveRecord::Base
  belongs_to :grammartest

  def next
    GrammartestQuestion.where("grammartest_questions.id > ?", self.id).order("grammartest_questions.id ASC").limit(1)
  end

  def previous
    GrammartestQuestion.where("grammartest_questions.id < ?", self.id).order("grammartest_questions.id DESC").limit(1)
  end
end
