class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]


 # validates :name,presence: true

  has_many :questions, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_many :answer_comments, dependent: :destroy
  has_many :comments, dependent: :destroy

  has_many :grade_orts, dependent: :destroy

  has_many :friendships, dependent: :destroy
  has_many :friends, through: :friendships, dependent: :destroy

  has_many :vote_users, dependent: :destroy
  has_many :votes, through: :vote_users, dependent: :destroy

  has_many :vote_user_answers, dependent: :destroy
  has_many :vote_answers, through: :vote_user_answers, dependent: :destroy

  has_many :user_mathtests, dependent: :destroy
  has_many :user_analogytests, dependent: :destroy
  has_many :user_grammartests, dependent: :destroy
  has_many :user_readingtests, dependent: :destroy

  def self.from_omniauth(auth,contact , user_friends)


    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|


      user.image = auth.info.image
      user.name = auth.info.name
      user.email = contact.email
      user.Locale = "ru"
      user.fb_access_token = auth.credentials.token
      user.password = Devise.friendly_token[0,20]
      user.save!
      user
    end


=begin
    user = find_or_create_by(uid: auth.uid, provider: auth.provider)
    user.name = auth.info.name
    user.email = "#{auth.info.name.downcase.delete(' ')}@gmail.com"
    user.Locale = "ru"
    user.password = Devise.friendly_token[0,20]
    user.save!
    user
=end
  end

  def facebook
    @facebook ||=  Koala::Facebook::API.new(fb_access_token)
  end

  def self.from_omniauth_google(auth)


    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|

      user.image = auth.info.image
      user.first_name = auth.info.first_name
      user.last_name = auth.info.last_name
      user.name = auth.info.name
      user.email = auth.info.email
      user.Locale = "ru"
      user.password = Devise.friendly_token[0,20]
      user.save!
      user
    end


=begin
    user = find_or_create_by(uid: auth.uid, provider: auth.provider)
    user.name = auth.info.name
    user.email = "#{auth.info.name.downcase.delete(' ')}@gmail.com"
    user.Locale = "ru"
    user.password = Devise.friendly_token[0,20]
    user.save!
    user
=end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

end
