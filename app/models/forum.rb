class Forum < ActiveRecord::Base
  has_many :questions, dependent: :destroy
  has_attached_file :image, :styles => { :medium =>     "300x300#", :thumb => "200x200#" }
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  def to_s
    id
  end
end
