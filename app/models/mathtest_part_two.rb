class MathtestPartTwo < ActiveRecord::Base
  belongs_to :mathtest


  def next
    MathtestPartTwo.where("mathtest_part_twos.id > ?", self.id).order("mathtest_part_twos.id ASC").limit(1)
  end

  def previous
    MathtestPartTwo.where("mathtest_part_twos.id < ?", self.id).order("mathtest_part_twos.id DESC").limit(1)
  end

end
