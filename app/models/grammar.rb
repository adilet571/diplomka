class Grammar < ActiveRecord::Base
  belongs_to :ort


  def next
    Grammar.where("grammars.id > ?", self.id).order("grammars.id ASC").limit(1)
  end

  def previous
    Grammar.where("grammars.id < ?", self.id).order("grammars.id DESC").limit(1)
  end
end
