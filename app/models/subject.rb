class Subject < ActiveRecord::Base
  has_many :subject_resources, dependent: :destroy
end
