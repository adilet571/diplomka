class ReadingtestQuestion < ActiveRecord::Base
  belongs_to :articletest
  belongs_to :readingtest


  def next
    ReadingtestQuestion.where("readingtest_questions.id > ?", self.id).order("readingtest_questions.id ASC").limit(1)
  end

  def previous
    ReadingtestQuestion.where("readingtest_questions.id < ?", self.id).order("readingtest_questions.id DESC").limit(1)
  end
end
