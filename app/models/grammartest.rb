class Grammartest < ActiveRecord::Base
  has_many :grammartest_questions, dependent: :destroy
  has_many :user_grammartests, dependent: :destroy

  def to_s
    name
  end
end
