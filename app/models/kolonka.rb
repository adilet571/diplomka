class Kolonka < ActiveRecord::Base
  belongs_to :ort


  def next
    Kolonka.where("kolonkas.id > ?", self.id).order("kolonkas.id ASC").limit(1)
  end

  def previous
    Kolonka.where("kolonkas.id < ?", self.id).order("kolonkas.id DESC").limit(1)
  end
end
