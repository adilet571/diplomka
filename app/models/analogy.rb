class Analogy < ActiveRecord::Base
  belongs_to :ort

  def next
    Analogy.where("analogies.id > ?", self.id).order("analogies.id ASC").limit(1)
  end

  def previous
    Analogy.where("analogies.id < ?", self.id).order("analogies.id DESC").limit(1)
  end
end
