class AnalogytestQuestion < ActiveRecord::Base
  belongs_to :analogytest


  def next
    AnalogytestQuestion.where("analogytest_questions.id > ?", self.id).order("analogytest_questions.id ASC").limit(1)
  end

  def previous
    AnalogytestQuestion.where("analogytest_questions.id < ?", self.id).order("analogytest_questions.id DESC").limit(1)
  end
end
