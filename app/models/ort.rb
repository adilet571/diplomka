class Ort < ActiveRecord::Base

  has_many :analogies, dependent: :destroy
  has_many :algebras, dependent: :destroy
  has_many :grammars, dependent: :destroy
  has_many :readings, dependent: :destroy
  has_many :grade_orts, dependent: :destroy
  has_many :kolonkas, dependent: :destroy

end
