class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :user
  has_many :answer_comments, dependent: :destroy
  validates :content, presence: true
  has_one :vote_answer, dependent: :destroy

  def to_s
    id
  end

end
