class Mathtest < ActiveRecord::Base
  has_many :mathtest_part_ones, dependent: :destroy
  has_many :mathtest_part_twos, dependent: :destroy

  has_many :user_mathtests, dependent: :destroy


  def to_s
    name
  end
end

