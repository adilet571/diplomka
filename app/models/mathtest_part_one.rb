class MathtestPartOne < ActiveRecord::Base
  belongs_to :mathtest



  def next
    MathtestPartOne.where("mathtest_part_ones.id > ?", self.id).order("mathtest_part_ones.id ASC").limit(1)
  end

  def previous
    MathtestPartOne.where("mathtest_part_ones.id < ?", self.id).order("mathtest_part_ones.id DESC").limit(1)
  end
end
