class Algebra < ActiveRecord::Base
  belongs_to :ort

  def next
    Algebra.where("algebras.id > ?", self.id).order("algebras.id ASC").limit(1)
  end

  def previous
    Algebra.where("algebras.id < ?", self.id).order("algebras.id DESC").limit(1)
  end
end
