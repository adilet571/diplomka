class Reading < ActiveRecord::Base
  belongs_to :ort
  belongs_to :article

  def next
    Reading.where("readings.id > ?", self.id).order("readings.id ASC").limit(1)
  end

  def previous
    Reading.where("readings.id < ?", self.id).order("readings.id DESC").limit(1)
  end
end
