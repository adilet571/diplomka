class Article < ActiveRecord::Base
  has_many :readings, dependent: :destroy
end
