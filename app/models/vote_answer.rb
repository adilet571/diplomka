class VoteAnswer < ActiveRecord::Base
  belongs_to :answer
  has_many :vote_user_answers, dependent: :destroy
  has_many :users, through: :vote_user_answers, dependent: :destroy

end
