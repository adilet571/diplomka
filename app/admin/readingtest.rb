ActiveAdmin.register Readingtest do
  menu :parent => "Окуу жана Түшүнүү"
  permit_params :name, :locale, :timer

  form html:{ multipart: true }  do |f|
    f.inputs 'Окуу жана Түшүнү Тест' do
      f.input :name, label: "Аты"
      f.input :locale,  as: :select, collection: ["kg","ru"] ,label: "Текст"
      f.input :timer
    end
    actions
  end
end
