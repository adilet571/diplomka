ActiveAdmin.register UserAnalogytest do
  menu :parent => 'Аналогия'

  index do
    column "Id", :id
    column "Аналогия Тест", :analogytest
    column "Колдонуучу", :user
    column "Датасы", :created_at
    actions
  end
end
