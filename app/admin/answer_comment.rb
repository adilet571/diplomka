ActiveAdmin.register AnswerComment do
  menu :parent => "Форум"

  index do
    column :id
    column :answer
    column :user
    column :content
    column :created_at
    actions
  end

  permit_params :id, :content
end
