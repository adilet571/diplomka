ActiveAdmin.register Algebra do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


  menu label: 'Алгебра'

  menu :parent => "ЖРТ"

  permit_params :content, :a, :b, :c, :d, :e, :answer, :definition, :ort_id

  form html:{ multipart: true }  do |f|
    f.inputs 'Алгебра' do

      f.input :ort, label: "ЖРТ"
      f.input :content,  as: :ckeditor, label: "Суроо"
      f.input :a       , as: :ckeditor, label: "А"
      f.input :b       , as: :ckeditor, label: "Б"
      f.input :c       , as: :ckeditor, label: "В"
      f.input :d       , as: :ckeditor, label: "Г"
      f.input :e       , as: :ckeditor, label: "Д"
      f.input :definition, as: :ckeditor, label: "Жообу"
      f.input :answer, as: :select, collection: ['А', 'Б', 'В', 'Г', 'Д'], label: "Жооп"

    end
    actions
  end

  index do
    column "Id", :id
    column "ЖРТ", :ort
    column "Жообу", :answer
    column "Датасы", :created_at
    actions
  end


end
