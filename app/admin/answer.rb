ActiveAdmin.register Answer do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  menu label: 'Жооптор'
  menu :parent => "Форум"

  index do
    column :id
    column :question
    column :user
    column :grade
    column :created_at
    actions
  end


  form html:{ multipart: true }  do |f|
    f.inputs 'Жооп' do

      f.input :content, as: :ckeditor

    end
    actions
  end

  permit_params :content, :grade, :created_at

end
