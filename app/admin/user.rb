ActiveAdmin.register User do
  menu label: 'Колдонуучулар'

  index do
    id_column
    column :name
    column :current_sign_in_at
    column :current_sign_in_ip
    column :last_sign_in_at
    column :provider
    column :sign_in_count
    actions
  end

  show do
    attributes_table do
      row :image do
        image_tag user.image
      end
      row :id
      row :name
      row :pol
      row :current_sign_in_at
      row :current_sign_in_ip
      row :last_sign_in_at
      row :last_sign_in_ip
      row :email
      row :first_name
      row :sign_in_count
      row :provider
      row :remember_created_at
      row :reset_password_sent_at
      row :reset_password_token
      row :uid
      row :created_at
      row :updated_at
    end
  end
end
