ActiveAdmin.register Tag do
  menu label: 'Теги'
  menu :parent => "Форум"

  index do
    column :id
    column :name
    column :created_at
    actions
  end
end
