ActiveAdmin.register UserGrammartest do
  menu :parent => 'Грамматика'

  index do
    column "Id", :id
    column "Грамматика Тест", :grammartest
    column "Колдонуучу", :user
    column "Датасы", :created_at
    actions
  end
end
