ActiveAdmin.register MathtestPartTwo do
  menu :parent => 'Математика'
  permit_params :content, :a, :b, :c, :d, :e, :answer, :definition, :mathtest_id

  form html:{ multipart: true }  do |f|
    f.inputs 'Алгебра' do
      f.input :mathtest
      f.input :content,  as: :ckeditor, label: "Суроо"
      f.input :a       , as: :ckeditor, label: "А"
      f.input :b       , as: :ckeditor, label: "Б"
      f.input :c       , as: :ckeditor, label: "В"
      f.input :d       , as: :ckeditor, label: "Г"
      f.input :e       , as: :ckeditor, label: "Д"
      f.input :definition, as: :ckeditor, label: "Жообу"
      f.input :answer, as: :select, collection: ['А', 'Б', 'В', 'Г', 'Д'], label: "Жооп"
    end
    actions
  end

  index do
    column "Id", :id
    column "ЖРТ", :mathtest
    column "Жообу", :answer
    column "Датасы", :created_at
    actions
  end
end
