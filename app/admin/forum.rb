ActiveAdmin.register Forum do
  permit_params :title, :image
  menu label: 'Форум'
  menu :parent => "Форум"

  form do |f|
    f.inputs 'Форум' do
      f.input :title, class: 'form-control'
      f.input :image, :required => false, :as => :file
    end
    f.actions
  end

  show do |forum|
    attributes_table do
      row :title
      row :image do
        image_tag(forum.image.url(:thumb))
      end
    end
  end

  index do
    column :id
    column :title
    column :image
    column :created_at
    actions
  end
end
