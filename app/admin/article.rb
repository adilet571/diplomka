ActiveAdmin.register Article do
  menu label: 'Текст'
  menu :parent => "ЖРТ"
  permit_params :name, :content

  form html:{ multipart: true }  do |f|
    f.inputs 'Текст' do
      f.input :name, label: "Аты"
      f.input :content,  as: :ckeditor, label: "Текст"
    end
    actions
  end

  index do
    column :id
    column :name
    column :created_at
    actions
  end
end
