ActiveAdmin.register GrammartestQuestion do
  menu :parent => 'Грамматика'
  permit_params :content, :a, :b, :c, :d, :answer, :definition, :grammartest_id

  form html:{ multipart: true }  do |f|
    f.inputs 'Грамматика' do
      f.input :grammartest, label: "Грамматика тест"
      f.input :content,  as: :ckeditor, label: "Суроо"
      f.input :a       , as: :ckeditor, label: "А"
      f.input :b       , as: :ckeditor, label: "Б"
      f.input :c       , as: :ckeditor, label: "В"
      f.input :d       , as: :ckeditor, label: "Г"
      f.input :definition, as: :ckeditor, label: "Жообу"
      f.input :answer, as: :select, collection: ['А', 'Б', 'В', 'Г'], label: "Жооп"
    end
    actions
  end

  index do
    column :id
    column :grammartest
    column :content
    column :answer
    column :created_at
    actions
  end
end
