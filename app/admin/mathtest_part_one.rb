ActiveAdmin.register MathtestPartOne do
  menu :parent => 'Математика'
  permit_params :content, :a, :b, :c, :d, :answer, :definition, :mathtest_id

  form html:{ multipart: true }  do |f|
    f.inputs 'Колонка' do
      f.input :mathtest, lable: "ЖРТ"
      f.input :content,  as: :ckeditor, label: "Суроо"
      f.input :definition, as: :ckeditor, label: "Жообу"
      f.input :answer, as: :select, collection: ['А', 'Б', 'В', 'Г'], label: "Жооп"
    end
    actions
  end

  index do
    column "Id", :id
    column "ЖРТ", :mathtest
    column "Жообу", :answer
    column "Датасы", :created_at
    actions
  end
end
