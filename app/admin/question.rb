ActiveAdmin.register Question do
  menu :label => 'Суроолор'
  menu :parent => "Форум"

  index do
    column :id
    column :title
    column :grade
    column :view
    column :user
    column :created_at
    actions
  end

  form html:{ multipart: true }  do |f|
    f.inputs 'Суроолор' do
      f.input :user
      f.input :forum_id
      f.input :title
      f.input :content, as: :ckeditor
    end
    actions
  end
  permit_params :title, :grade, :view, :created_at, :content
end
