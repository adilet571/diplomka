ActiveAdmin.register SubjectResource do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  menu :parent => "Окуу Ресурстары"
  permit_params :subject_id, :video_url, :name


  form html:{ multipart: true }  do |f|
    f.inputs 'Видео кошуу' do
      f.input :subject, label: "Предмет"
      f.input :name, label: "Аты"
      f.input :video_url, label: 'Видео шилтемеси'
    end
    actions
  end
end
