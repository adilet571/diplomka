ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc { ("Башкаруу") }

  content title: proc { "Башкаруу" } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
    end

    section "Акыркы" do
      table_for User.order("created_at desc").limit(5) do
        column "Аты", :name do |user|
          link_to user.name, [:admin, user]
        end
        column "Катталган датасы", :created_at
        column "Почта", :email
        column "Акыркы Системага кирген убактысы", :last_sign_in_at
      end
      strong { link_to "Бардык Колдонуучулар", admin_users_path }
    end

    columns do
      column do
        panel "Жалпы колдонуучулар" do
          ul do
            users = User.count(:all)
            h2 users

          end
        end
      end

      column do
        panel "ЖРТ" do
          ort = Ort.count(:all)
          h2 " ЖРТ жалпы тесттердин саны: " + ort.to_s
        end
      end

      column do
        panel "Форумдагы Суроолор" do
          question = Question.count(:all)
          h2 " Жалпы суралган суроолор: " + question.to_s
        end
      end
    end
  end
end
