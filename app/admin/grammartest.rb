ActiveAdmin.register Grammartest do
  menu :parent => 'Грамматика'
  permit_params :name, :locale, :timer

  form html:{ multipart: true }  do |f|
    f.inputs 'Грамматика Тест' do
      f.input :name, label: "Аты"
      f.input :locale,  as: :select, collection: ["kg","ru"] ,label: "Текст"
      f.input :timer
    end
    actions
  end
end
