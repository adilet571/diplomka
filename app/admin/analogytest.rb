ActiveAdmin.register Analogytest do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  menu :parent => 'Аналогия'


  permit_params :name, :locale, :timer

  form html:{ multipart: true }  do |f|
    f.inputs 'Аналогия Тест' do

      f.input :name, label: "Тест N:"
      f.input :locale,  as: :select, collection: ["kg","ru"] ,label: "Текст"
      f.input :timer

    end
    actions
  end

end
