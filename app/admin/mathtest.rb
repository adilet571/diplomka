ActiveAdmin.register Mathtest do
  menu :parent => 'Математика'
  permit_params :name, :locale, :timer

  form html:{ multipart: true }  do |f|
    f.inputs 'Метематика Тест' do
      f.input :name, label: "Аты"
      f.input :locale,  as: :select, collection: ["kg","ru"] ,label: "Текст"
      f.input :timer, label: 'Тесттин убактысы'
    end
    actions
  end
end
