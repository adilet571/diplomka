ActiveAdmin.register UserMathtest do
  menu :parent => 'Математика'

  index do
    column "Id", :id
    column "Математика Тест", :mathtest
    column "Колдонуучу", :user
    column "Датасы", :created_at
    actions
  end
end
