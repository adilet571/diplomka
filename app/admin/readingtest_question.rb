ActiveAdmin.register ReadingtestQuestion do
  menu :parent => "Окуу жана Түшүнүү"
  permit_params :content, :a, :b, :c, :d, :answer, :definition, :readingtest_id, :articletest_id
  form html:{ multipart: true }  do |f|
    f.inputs 'Окуу жана Түшүнүү' do
      f.input :readingtest, label: "Тест N:"
      f.input :articletest, label: "Текст"
      f.input :content,  as: :ckeditor, label: "Суроо"
      f.input :a       , as: :ckeditor, label: "А"
      f.input :b       , as: :ckeditor, label: "Б"
      f.input :c       , as: :ckeditor, label: "В"
      f.input :d       , as: :ckeditor, label: "Г"
      f.input :definition, as: :ckeditor, label: "Жообу"
      f.input :answer, as: :select, collection: ['А', 'Б', 'В', 'Г'], label: "Жооп"
    end
    actions
  end

  index do
    column :id
    column :readingtest
    column :articletest
    column :answer
    column :created_at
    actions
  end
end
