ActiveAdmin.register GradeOrt do
  menu label: 'ЖРТ жыйынтыктар'
  menu :parent => "ЖРТ"
  permit_params :ort_id, :user_id,:result

  index do
    column :id
    column :user
    column :ort
    column :result
    column :created_at
    actions
  end
end
