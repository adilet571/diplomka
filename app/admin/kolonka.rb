ActiveAdmin.register Kolonka do
  menu label: 'Колонка'
  menu :parent => "ЖРТ"
  permit_params :content, :a, :b, :c, :d, :answer, :definition, :ort_id

  form html:{ multipart: true }  do |f|
    f.inputs 'Колонка' do
      f.input :ort, label: "ЖРТ"
      f.input :content,  as: :ckeditor, label: "Суроо"
      f.input :definition, as: :ckeditor, label: "Жообу"
      f.input :answer, as: :select, collection: ['А', 'Б', 'В', 'Г'], label: "Жооп"
    end
    actions
  end

  index do
    column "Id", :id
    column "ЖРТ", :ort
    column "Жообу", :answer
    column "Датасы", :created_at
    actions
  end
end
