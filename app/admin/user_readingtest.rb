ActiveAdmin.register UserReadingtest do
  menu :parent => 'Окуу жана Түшүнүү'

  index do
    column "Id", :id
    column "Окуу жана Түшүнүү Тест", :readingtest
    column "Колдонуучу", :user
    column "Датасы", :created_at
    actions
  end
end
