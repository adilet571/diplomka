class ModelMailer < ApplicationMailer

  default from: "noreplystudentkg@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.model_mailer.new_record_notification.subject
  #
  def new_record_notification(user,comment,to,url)
    @user = user
    @comment = comment
    @url = url
    mail to: to.email, subject: "#{@user.name} комментировал свой вопрос"
  end
end
