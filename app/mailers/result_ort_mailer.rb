class ResultOrtMailer < ApplicationMailer
  default from: "noreplystudentkg@gmail.com"

  def send_result(name, email, result)
    @name = name
    @result = result
    mail(to: email, subject: 'Сиздин ЖРТ баллыңыз!')
  end
end
