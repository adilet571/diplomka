class AnswerMailer < ApplicationMailer
  default from: "noreplystudentkg@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.model_mailer.new_answer_notification.subject
  #
  def new_answer_notification(user,answer,to,url,title)
    @user = user
    @title = title
    @answer = answer
    @url = url
    mail to: to.email, subject: "#{@user.name} ответил на ваш вопрос!"
  end
end
