module ApplicationHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def resource_class
    devise_mapping.to
  end

  def count_question
    if session[:count_question].nil?
      session[:count_question] = 0
    end
    session[:count_question] += 1
  end

  def discount_question
    if session[:count_question].nil?
      session[:count_question] = 0
    end
    if session[:count_question] > 0
      session[:count_question] -= 1
    end
  end

  def go_to_the_ort_part!

    if !session[:ort_part].nil?
      case session[:ort_part]
        when 'algebra'
          redirect_to algebra_ort_mains_path
          return
        when 'analogy'
          redirect_to analogy_ort_mains_path
          return
        when 'reading'
          redirect_to reading_ort_mains_path
          return
        when 'grammar'
          redirect_to grammar_ort_mains_path
          return
      end
    end
  end

  def question_answers_kolonka
    session[:question_answers_kolonka] ||= {}
  end

  def question_answers_algebra
    session[:question_answers_algebra] ||= {}
  end

  def question_answers_analogy
    session[:question_answers_analogy] ||= {}
  end

  def question_answers_reading
    session[:question_answers_reading] ||= {}
  end

  def question_answers_grammar
    session[:question_answers_grammar] ||= {}
  end

  def question_answers_part_one
    session[:question_answers_part_one] ||= {}
  end

  def question_answers_part_two
    session[:question_answers_part_two] ||= {}
  end

  def progress_question
    (session[:count_question]*100/session[:question_total])
  end
end
