class AnswersController < ApplicationController
  before_action :authenticate_user!, only: [:new,:vote_answer_down,:vote_answer_up, :create]

  def create
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @answer = @question.answers.new(answer_params)
    @answer.user_id = current_user.id
    @answer.save
    @answers = @question.answers
    to = @question.user
    @total = @question.answers.count
    AnswerMailer.new_answer_notification(current_user, @answer, to, request.url.chomp("/answers"), @question.title).deliver_now

  end

  def show
  end

  def edit

    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @answer = Answer.find(params[:id])

  end

  def vote_answer_up

    @answer = Answer.find(params[:format])
    user = @answer.user
    vote = @answer.vote_answer

    if vote.nil? && user != current_user
      vote = @answer.build_vote_answer
      vote.users << current_user
      @answer.grade += 1

      if @answer.save!
        vote.save!
      end
    elsif !vote.nil? && !vote.users.exists?(current_user) && user != current_user
      vote.users << current_user
      @answer.grade += 1

      if @answer.save!
        vote.save!
      end
    elsif user == current_user
      @message = 'You cannot vote up your Answer!'
    end
  end

  def vote_answer_down
    @answer = Answer.find(params[:format])
    user = @answer.user
    vote = @answer.vote_answer

    if vote.nil? && user != current_user
      vote = @answer.build_vote_answer
      vote.users << current_user
      @answer.grade -= 1

      if @answer.save!
        vote.save!
      end
    elsif !vote.nil? && !vote.users.exists?(current_user) && user != current_user
      vote.users << current_user
      @answer.grade -= 1

      if @answer.save!
        vote.save!
      end
    elsif user == current_user
      @message = 'You cannot vote down your Answer!'
    end
  end

  def update
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @answer = Answer.find(params[:id])

    @answer.update_attributes(answer_params)

    @answers = @question.answers
  end

  def new
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @answer = @question.answers.new
  end

  def delete
  end

  def destroy
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @answers = @question.answers
    @answer = Answer.find(params[:id])
    @total = @question.answers.count


    @answer.destroy
  end

  private
  def answer_params
    params.require(:answer).permit(:content)
  end
end
