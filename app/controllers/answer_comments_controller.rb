class AnswerCommentsController < ApplicationController

  before_action :authenticate_user!, only: [:new,:create,:destroy]

  def new
    @answer = Answer.find(params[:answer_id])
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @answer_comment= @answer.answer_comments.new
  end

  def index
  end
  def create

    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @answer = Answer.find(params[:answer_id])
    @answer_comment = @answer.answer_comments.new(answer_comment_params)
    @answer_comment.user_id = current_user.id

    @answer_comment.save
    to = @answer_comment.user
    AnswerCommentMailer.new_answer_comment_notification(current_user, @answer_comment, to, request.url.chomp("answers/#{@answer.id}/answer_comments")).deliver_later


    @answer_comments = @answer.answer_comments
  end

  def destroy

    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @answer = Answer.find(params[:answer_id])

    @answer_comments = @answer.answer_comments
    @answer_comment = AnswerComment.find(params[:id])

    @answer_comment.destroy
  end


  private
  def answer_comment_params
    params.require(:answer_comment).permit(:content)
  end
end
