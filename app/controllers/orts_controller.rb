class OrtsController < ApplicationController
  def index

    case session[:test]
      when 'analogytest'
        redirect_to ort_analogy_quizzes_path
        return
      when 'mathtest'
        redirect_to part_one_ort_mathematic_quizzes_path
        return
      when 'readingtest'
        redirect_to ort_reading_quizzes_path
        return
      when 'grammartest'
        redirect_to ort_grammar_quizzes_path
        return
      when 'ort'
        redirect_to kolonka_ort_mains_path
        return
      when 'email_ort'
        redirect_to email_ort_ort_mains_path
        return
    end

    @maths = Mathtest.where(locale: session[:locale])
    @analogies = Analogytest.where(locale: session[:locale])
    @grammars = Grammartest.where(locale: session[:locale])
    @readings = Readingtest.where(locale: session[:locale])
    @orts = Ort.where(locale: session[:locale])

  end
end
