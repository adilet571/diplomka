class ForumsController < ApplicationController

  before_action :authenticate_user!, only: [:new,:create]

  def index
    @categories = Forum.all

  end

  def new
    @question = Question.new
  end

  def create
  end

  def show
    @category = Forum.find(params[:id])
  end
end
