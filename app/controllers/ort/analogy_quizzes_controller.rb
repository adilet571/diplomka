class Ort::AnalogyQuizzesController < ApplicationController
  before_action :authenticate_user!
  include ApplicationHelper

  def index
    if session[:analogytest_id].nil?
      session[:analogytest_id] = params[:id]
      count_question
    end
    if session[:analogytest_id].nil?
      redirect_to orts_path
      return
    end
    session[:test] = "analogytest"
    @analogytest ||= Analogytest.find(session[:analogytest_id])
    @@analogies ||= @analogytest.analogytest_questions
    session[:question_timer] ||= @analogytest.timer
    session[:question_total] ||= @@analogies.length

    if session[:question_analogy_id].nil?
      session[:question_analogy_id] = @@analogies.first.id
    end
    if !session[:question_analogy_id].nil?
      @question = @@analogies.find(session[:question_analogy_id])
    end
    @id = question_answers_analogy["#{@question.id}"]
  end

  def previous_analogy
    @question = @@analogies.find(session[:question_analogy_id]).previous.first

    if !@question.nil? && @@analogies.exists?(@question)
      session[:question_analogy_id] = @question.id
      discount_question
      @id = question_answers_analogy["#{@question.id}"]
    end
  end

  def check_analogy
    question = @@analogies.find(session[:question_analogy_id])

    if question_answers_analogy.key?(question.id)
      question_answers_analogy[question.id] = params[:answer]
    else
      question_answers_analogy.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@analogies.exists?(question_next)
      session[:question_analogy_id] = question_next.id
    else
      session[:question_analogy_id] = nil
    end

    if session[:question_analogy_id].nil?
      render js: "window.location = '#{result_ort_analogy_quizzes_path}'"
    else
      count_question
      @question = question_next
      @id = question_answers_analogy["#{question_next.id}"]
    end
  end

  def start_over
    @result = 0
    @total = session[:question_total]

    question_answers_analogy.each do |key, value|
      question = @@analogies.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end
    destroy_session
  end

  def result
    @result = 0
    @total = session[:question_total]

    question_answers_analogy.each do |key, value|
      question = @@analogies.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end

    user_analogytest = current_user.user_analogytests.new
    user_analogytest.analogytest_id = session[:analogytest_id]
    user_analogytest.grade = @result
    user_analogytest.save!
    destroy_session
  end

  private
  def destroy_session
    session.delete(:analogytest_id)
    session.delete(:question_analogy_id)
    session.delete(:question_answers_analogy)

    session.delete(:count_question)
    session.delete(:question_total)
    session.delete(:question_timer)
    session.delete(:test)
    @@analogies = nil
  end
end
