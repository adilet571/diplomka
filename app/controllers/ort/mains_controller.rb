class Ort::MainsController < ApplicationController
  before_action :authenticate_user!, only: [:kolonka]
  include ApplicationHelper

  def kolonka
    go_to_the_ort_part!

    if session[:ort_id].nil?
      session[:ort_id] = params[:id]
    end

    if session[:ort_id].nil?
      redirect_to orts_path
      return
    end

    set_question

    session[:test] = 'ort'
    session[:ort_part] = 'kolonka'
    session[:question_timer] ||= @@ort.timer
    session[:question_total] ||= @@kolonkas.length + @@algebras.length + @@analogies.length + @@readings.length + @@grammars.length

    if session[:question_kolonka_id].nil?
      session[:question_kolonka_id] = @@kolonkas.first.id
      count_question
    end

    if !session[:question_kolonka_id].nil?
      begin
        @question = @@kolonkas.find(session[:question_kolonka_id])
        @id = question_answers_kolonka["#{@question.id}"]
      rescue ActiveRecord::RecordNotFound
        puts 'ort.kolonkas not found with id=' + session[:question_kolonka_id].to_s
      end
    end
  end

  def email_ort
    go_to_the_ort_part!

    if !params[:name].nil? && !params[:email].nil?
      #params[:id] = Ort.where("RAND()").first.id
      params[:id] = 1

      session[:name] = params[:name]
      session[:email] = params[:email]
    end

    if session[:ort_id].nil?
      session[:ort_id] = params[:id]
    end

    if session[:ort_id].nil?
      redirect_to orts_path
      return
    end

    session[:test] = 'email_ort'
    session[:ort_part] = 'email_ort'

    set_question

    session[:question_timer] ||= @@ort.timer
    session[:question_total] ||= @@kolonkas.length + @@algebras.length + @@analogies.length + @@readings.length + @@grammars.length

    if session[:question_kolonka_id].nil?
      session[:question_kolonka_id] = @@kolonkas.first.id
      count_question
    end

    if !session[:question_kolonka_id].nil?
      @question = @@kolonkas.find(session[:question_kolonka_id])
    end

    @id = question_answers_kolonka["#{@question.id}"]
  end

  def previous_kolonka
    @question = @@kolonkas.find(session[:question_kolonka_id]).previous.first

    if !@question.nil? && @@kolonkas.exists?(@question)
      session[:question_kolonka_id] = @question.id
      discount_question
      @id = question_answers_kolonka["#{@question.id}"]
    end
  end

  def check_kolonka
    begin
      question = @@kolonkas.find(session[:question_kolonka_id])
    rescue ActiveRecord::RecordNotFound
      puts 'ort.kolonkas in main not found with id='+session[:question_kolonka_id].to_s
    end
    # store answers to session hash
    if question_answers_kolonka.key?(question.id)
      question_answers_kolonka[question.id] = params[:answer]
    else
      question_answers_kolonka.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@kolonkas.exists?(question_next)
      session[:question_kolonka_id] = question_next.id
    else
      session[:question_kolonka_id] = nil
    end

    if session[:question_kolonka_id].nil?
      render js: "window.location = '#{algebra_ort_mains_path}'"
    else
      count_question
      @question = question_next
      @id = question_answers_kolonka["#{question_next.id}"]
    end
  end

  def algebra
    if session[:ort_id].nil?
      redirect_to orts_path
      return
    end

    @@ort = Ort.find(session[:ort_id])
    @@algebras ||= @@ort.algebras

    session[:ort_part] = 'algebra'

    if session[:question_algebra_id].nil?
      session[:question_algebra_id] = @@algebras.first.id
      count_question
    end

    if !session[:question_algebra_id].nil?
      @question = @@algebras.find(session[:question_algebra_id])
    end

    @id = question_answers_algebra["#{@question.id}"]
  end

  def previous_algebra
    @question = @@algebras.find(session[:question_algebra_id]).previous.first

    if !@question.nil? && @@algebras.exists?(@question)
      session[:question_algebra_id] = @question.id
      discount_question
      @id = question_answers_algebra["#{@question.id}"]
    end
  end

  def check_algebra
    begin
      question = @@algebras.find(session[:question_algebra_id])
    rescue ActiveRecord::RecordNotFound
      puts 'ort.algebras not found with id=' + session[:question_algebra_id].to_s
    end

    if question_answers_algebra.key?(question.id)
      question_answers_algebra[question.id] = params[:answer]
    else
      question_answers_algebra.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@algebras.exists?(question_next)
      session[:question_algebra_id] = question_next.id
    else
      session[:question_algebra_id] = nil
    end

    if session[:question_algebra_id].nil?
      render js: "window.location = '#{analogy_ort_mains_path}'"
    else
      count_question
      @question = question_next
      @id = question_answers_algebra["#{question_next.id}"]
    end
  end

  def analogy
    @@ort = Ort.find(session[:ort_id])
    @@analogies ||= @@ort.analogies

    session[:ort_part] = 'analogy'

    if session[:question_analogy_id].nil?
      session[:question_analogy_id] = @@analogies.first.id
      count_question
    end

    if !session[:question_analogy_id].nil?
      @question = @@analogies.find(session[:question_analogy_id])
    end

    @id = question_answers_analogy["#{@question.id}"]
  end

  def previous_analogy
    @question = @@analogies.find(session[:question_analogy_id]).previous.first

    if !@question.nil? && @@analogies.exists?(@question)
      session[:question_analogy_id] = @question.id
      discount_question
      @id = question_answers_analogy["#{@question.id}"]
    end
  end

  def check_analogy
    begin
      question = @@analogies.find(session[:question_analogy_id])
    rescue ActiveRecord::RecordNotFound
      puts 'ort.analogies not found with id=' + session[:question_analogy_id].to_s
    end

    # store answers to session hash
    if question_answers_analogy.key?(question.id)
      question_answers_analogy[question.id] = params[:answer]
    else
      question_answers_analogy.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@analogies.exists?(question_next)
      session[:question_analogy_id] = question_next.id
    else
      session[:question_analogy_id] = nil
    end

    if session[:question_analogy_id].nil?
      render js: "window.location = '#{reading_ort_mains_path}'"
    else
      count_question
      @question = question_next
      @id = question_answers_analogy["#{question_next.id}"]
    end
  end


  def reading
    ort = Ort.find(session[:ort_id])
    @@readings ||= ort.readings

    session[:ort_part] = 'reading'

    if session[:question_reading_id].nil?
      session[:question_reading_id] = @@readings.first.id
      count_question
    end

    if !session[:question_reading_id].nil?
      @question = @@readings.find(session[:question_reading_id])
      @article = @question.article
    end

    @id = question_answers_reading["#{@question.id}"]
  end

  def check_reading
    begin
      question = @@readings.find(session[:question_reading_id])
    rescue ActiveRecord::RecordNotFound
      puts 'ort.readings not found with id=' + session[:question_reading_id].to_s
    end

    # store answers to session hash
    if question_answers_reading.key?(question.id)
      question_answers_reading[question.id] = params[:answer]
    else
      question_answers_reading.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@readings.exists?(question_next)
      session[:question_reading_id] = question_next.id
    else
      session[:question_reading_id] = nil
    end

    if session[:question_reading_id].nil?
      render js: "window.location = '#{grammar_ort_mains_path}'"
    else
      count_question
      @question = question_next
      @article = @question.article
      @id = question_answers_reading["#{question_next.id}"]
    end
  end

  def previous_reading
    @question = @@readings.find(session[:question_reading_id]).previous.first
    if !@question.nil?
      @article = @question.article
    end

    if !@question.nil? && @@readings.exists?(@question)
      session[:question_reading_id] = @question.id
      discount_question
      @id = question_answers_reading["#{@question.id}"]
    end
  end

  def grammar
    ort = Ort.find(session[:ort_id])
    @@grammars ||= ort.grammars

    session[:ort_part] = 'grammar'

    if session[:question_grammar_id].nil?
      session[:question_grammar_id] = @@grammars.first.id
      count_question
    end

    if !session[:question_grammar_id].nil?
      @question = @@grammars.find(session[:question_grammar_id])
    end

    @id = question_answers_grammar["#{@question.id}"]
  end

  def previous_grammar
    @question = @@grammars.find(session[:question_grammar_id]).previous.first

    if !@question.nil? && @@grammars.exists?(@question)
      session[:question_grammar_id] = @question.id
      discount_question
      @id = question_answers_grammar["#{@question.id}"]
    end
  end

  def check_grammar
    begin
      question = @@grammars.find(session[:question_grammar_id])
    rescue ActiveRecord::RecordNotFound
      puts 'ort.grammars not found with id=' + session[:question_grammar_id].to_s
    end

    # store answers to session hash
    if question_answers_grammar.key?(question.id)
      question_answers_grammar[question.id] = params[:answer]
    else
      question_answers_grammar.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@grammars.exists?(question_next)
      session[:question_grammar_id] = question_next.id
    else
      session[:question_grammar_id] = nil
    end

    if session[:question_grammar_id].nil?
      render js: "window.location = '#{result_ort_mains_path}'"
    else
      count_question
      @question = question_next
      @id = question_answers_grammar["#{question_next.id}"]
    end
  end

  def result
    @result = check_question
    @total = session[:question_total]
    @is_send_email = false

    if !session[:name].nil? && !session[:email].nil?
      #send rmail
      ResultOrtMailer.send_result(session[:name], session[:email], @result).deliver_later
      @is_send_email = true
      @email = session[:email]
    else
      user_grade_ort = current_user.grade_orts.new
      user_grade_ort.ort_id = session[:ort_id]
      user_grade_ort.result = @result
      user_grade_ort.save!
    end
    destroy_session
  end

  def start_over
    @result = check_question
    @total = session[:question_total]
    @is_send_email = false

    if !session[:name].nil? && !session[:email].nil?
      ResultOrtMailer.send_result(session[:name], session[:email], @result).deliver_later
      @is_send_email = true
      @email = session[:email]
    end

    destroy_session
  end

  private
  def check_question
    result = 0
    question_answers_reading.each do |key, value|
      question = @@readings.find(key)
      if question.answer == value
        result = result + 1
      end
    end

    question_answers_grammar.each do |key, value|
      question = @@grammars.find(key)
      if question.answer == value
        result = result + 1
      end
    end

    question_answers_analogy.each do |key, value|
      question = @@analogies.find(key)
      if question.answer == value
        result = result + 1
      end
    end

    question_answers_algebra.each do |key, value|
      question = @@algebras.find(key)
      if question.answer == value
        result = result + 1
      end
    end

    question_answers_kolonka.each do |key, value|
      question = @@kolonkas.find(key)
      if question.answer == value
        result = result + 1
      end
    end
    result
  end

  def destroy_session
    session.delete(:email_ort)
    session.delete(:name)
    session.delete(:email)
    session.delete(:ort_part)
    session.delete(:ort_id)
    session.delete(:test)
    session.delete(:question_answers_kolonka)
    session.delete(:question_answers_algebra)
    session.delete(:question_answers_analogy)
    session.delete(:question_answers_reading)
    session.delete(:question_answers_grammar)
    session.delete(:question_kolonka_id)
    session.delete(:question_algebra_id)
    session.delete(:question_analogy_id)
    session.delete(:question_reading_id)
    session.delete(:question_grammar_id)
    session.delete(:question_timer)
    session.delete(:question_total)
    session.delete(:count_question)

    @@ort = nil
    @@kolonkas = nil
    @@algebras = nil
    @@analogies = nil
    @@readings = nil
    @@grammars = nil
  end

  def set_question
    @@ort ||= Ort.find(session[:ort_id])
    @@kolonkas ||= @@ort.kolonkas
    @@algebras ||= @@ort.algebras
    @@analogies ||= @@ort.analogies
    @@readings ||= @@ort.readings
    @@grammars ||= @@ort.grammars
  end
end
