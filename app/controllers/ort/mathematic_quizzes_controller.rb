class Ort::MathematicQuizzesController < ApplicationController
  before_action :authenticate_user!
  include ApplicationHelper

  def part_one

    if session[:mathtest_id].nil?
      session[:mathtest_id] = params[:id]
      count_question
    end

    if session[:mathtest_id].nil?
      redirect_to orts_path
      return
    end

    session[:test] = 'mathtest'

    @mathtest ||= Mathtest.find(session[:mathtest_id])

    @@part_twos ||= @mathtest.mathtest_part_twos
    @@part_ones ||= @mathtest.mathtest_part_ones

    session[:question_timer] ||= @mathtest.timer

    session[:question_total] ||= @@part_ones.length + @@part_twos.length

    if session[:question_part_one_id].nil? && session[:question_part_two_id].nil?
      session[:question_part_one_id] = @@part_ones.first.id
    end

    if !session[:question_part_one_id].nil?
      @question = @@part_ones.find(session[:question_part_one_id])
    end

    # this action redirec_to second part mathematics when part one finished
    if !session[:question_part_two_id].nil?
      redirect_to part_two_ort_mathematic_quizzes_path
      return
    end

    @id = question_answers_part_one["#{@question.id}"]
  end

  def part_two

    if session[:mathtest_id].nil?
      redirect_to orts_path
      return
    end

    @mathtest = Mathtest.find(session[:mathtest_id])
    @@part_twos ||= @mathtest.mathtest_part_twos

    if session[:question_part_two_id].nil?
      session[:question_part_two_id] = @@part_twos.first.id
      count_question
    end

    @question = @@part_twos.find(session[:question_part_two_id])
    @id = question_answers_part_two["#{@question.id}"]
  end

  def previous_part_one
    @question = @@part_ones.find(session[:question_part_one_id]).previous.first

    if !@question.nil? && @@part_ones.exists?(@question)
      session[:question_part_one_id] = @question.id
      discount_question
      @id = question_answers_part_one["#{@question.id}"]
    end
  end

  def previous_part_two
    @question = @@part_twos.find(session[:question_part_two_id]).previous.first

    if !@question.nil? && @@part_twos.exists?(@question)
      session[:question_part_two_id] = @question.id
      discount_question
      @id = question_answers_part_two["#{@question.id}"]
    end
  end

  def check_part_one
    question = @@part_ones.find(session[:question_part_one_id])

    # store answers to session hash
    if question_answers_part_one.key?(question.id)
      question_answers_part_one[question.id] = params[:answer]
    else
      question_answers_part_one.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@part_ones.exists?(question_next)
      session[:question_part_one_id] = question_next.id
    else
      session[:question_part_one_id] = nil
    end

    if session[:question_part_one_id].nil?
      render js: "window.location = '#{part_two_ort_mathematic_quizzes_path}'"
    else
      count_question
      @question = question_next
      @id = question_answers_part_one["#{question_next.id}"]
    end
  end

  def check_part_two
    question = @@part_twos.find(session[:question_part_two_id])

    if question_answers_part_two.key?(question.id)
      question_answers_part_two[question.id] = params[:answer]
    else
      question_answers_part_two.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@part_twos.exists?(question_next)
      session[:question_part_two_id] = question_next.id
    else
      session[:question_part_two_id] = nil
    end

    if session[:question_part_two_id].nil?
      render js: "window.location = '#{result_ort_mathematic_quizzes_path}'"
    else
      count_question
      @question = question_next
      @id = question_answers_part_two["#{question_next.id}"]
    end
  end

  def result
    @result = 0
    @total = session[:question_total]

    question_answers_part_two.each do |key, value|
      question = @@part_twos.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end

    question_answers_part_one.each do |key, value|
      question = @@part_ones.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end

    user_mathtest = current_user.user_mathtests.new
    user_mathtest.mathtest_id = session[:mathtest_id]
    user_mathtest.grade = @result
    user_mathtest.save!

    destroy_sessions
  end

  def start_over
    @result = 0
    @total = session[:question_total]

    question_answers_part_one.each do |key, value|
      question = @@part_ones.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end

    question_answers_part_two.each do |key, value|
      question = @@part_twos.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end

    destroy_sessions
  end

  private
    def destroy_sessions
      question_answers_part_two = nil
      question_answers_part_one = nil
      session.delete(:question_part_one_id)
      session.delete(:question_answers_part_one)
      session.delete(:question_answers_part_two)
      session.delete(:mathtest_id)
      session.delete(:question_part_two_id)
      session.delete(:count_question)
      session.delete(:question_total)
      session.delete(:question_timer)
      session.delete(:test)
      @@part_ones = nil
      @@part_twos = nil
    end
end
