class Ort::ReadingQuizzesController < ApplicationController
  before_action :authenticate_user!
  include ApplicationHelper

  def index
    if session[:readingtest_id].nil?
      session[:readingtest_id] = params[:id]
      count_question
    end

    if session[:readingtest_id].nil?
      redirect_to orts_path
      return
    end

    session[:test] = "readingtest"

    @readingtest ||= Readingtest.find(session[:readingtest_id])
    @@readings ||= @readingtest.readingtest_questions

    session[:question_timer] ||= @readingtest.timer
    session[:question_total] ||= @@readings.length

    if session[:question_reading_id].nil?
      session[:question_reading_id] = @@readings.first.id
    end

    if !session[:question_reading_id].nil?
      @question = @@readings.find(session[:question_reading_id])
      @article = @question.articletest
      @id = question_answers_reading["#{@question.id}"]
    end
  end

  def previous_reading
    @question = @@readings.find(session[:question_reading_id]).previous.first

    if !@question.nil?
      @article = @question.articletest
    end

    if !@question.nil? && @@readings.exists?(@question)
      session[:question_reading_id] = @question.id
      discount_question
      @id = question_answers_reading["#{@question.id}"]
    end
  end

  def check_reading
    question = @@readings.find(session[:question_reading_id])

    # store answers to session hash
    if question_answers_reading.key?(question.id)
      question_answers_reading[question.id] = params[:answer]
    else
      question_answers_reading.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@readings.exists?(question_next)
      session[:question_reading_id] = question_next.id
    else
      session[:question_reading_id] = nil
    end

    if session[:question_reading_id].nil?
      render js: "window.location = '#{result_ort_reading_quizzes_path}'"
    else
      count_question
      @question = question_next
      @article = @question.articletest
      @id = question_answers_reading["#{question_next.id}"]
    end
  end

  def result
    @result = 0
    @total = session[:question_total]

    question_answers_reading.each do |key, value|
      question = @@readings.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end

    user_readingtest = current_user.user_readingtests.new
    user_readingtest.readingtest_id = session[:readingtest_id]
    user_readingtest.grade = @result
    user_readingtest.save!
    destroy_session
  end

  def start_over
    @result = 0
    @total = session[:question_total]

    question_answers_reading.each do |key, value|
      question = @@readings.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end
    destroy_session
  end

  private
    def destroy_session
      session.delete(:readingtest_id)
      session.delete(:question_reading_id)
      session.delete(:question_answers_reading)

      session.delete(:count_question)
      session.delete(:question_total)
      session.delete(:question_timer)
      session.delete(:test)

      @@readings = nil
    end
end
