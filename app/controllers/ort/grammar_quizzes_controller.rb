class Ort::GrammarQuizzesController < ApplicationController
  before_action :authenticate_user!
  include ApplicationHelper

  def index
    if session[:grammartest_id].nil?
      session[:grammartest_id] = params[:id]
      count_question
    end
    if session[:grammartest_id].nil?
      redirect_to orts_path
      return
    end
    session[:test] = "grammartest"

    @grammartest ||= Grammartest.find(session[:grammartest_id])
    @@grammars ||= @grammartest.grammartest_questions

    session[:question_timer] ||= @grammartest.timer
    session[:question_total] ||= @@grammars.length
    if session[:question_grammar_id].nil?
      session[:question_grammar_id] = @@grammars.first.id
    end
    if !session[:question_grammar_id].nil?
      @question = @@grammars.find(session[:question_grammar_id])
    end
    @id = question_answers_grammar["#{@question.id}"]
  end

  def previous_grammar
    @question = @@grammars.find(session[:question_grammar_id]).previous.first

    if !@question.nil? && @@grammars.exists?(@question)
      session[:question_grammar_id] = @question.id
      discount_question
      @id = question_answers_grammar["#{@question.id}"]
    end
  end

  def check_grammar
    question = @@grammars.find(session[:question_grammar_id])
    # store answers to session hash
    if question_answers_grammar.key?(question.id)
      question_answers_grammar[question.id] = params[:answer]
    else
      question_answers_grammar.store(question.id, params[:answer])
    end

    question_next = question.next.first

    if !question_next.nil? && @@grammars.exists?(question_next)
      session[:question_grammar_id] = question_next.id
    else
      session[:question_grammar_id] = nil
    end

    if session[:question_grammar_id].nil?
      render js: "window.location = '#{result_ort_grammar_quizzes_path}'"
    else
      count_question
      @question = question_next
      @id = question_answers_grammar["#{question_next.id}"]
    end
  end

  def start_over
    @result = 0
    @total = session[:question_total]

    question_answers_grammar.each do |key, value|
      question = @@grammars.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end
    destroy_session
  end

  def result
    @result = 0
    @total = session[:question_total]

    question_answers_grammar.each do |key, value|
      question = @@grammars.find(key)
      if question.answer == value
        @result = @result + 1
      end
    end

    user_grammartest = current_user.user_grammartests.new
    user_grammartest.grammartest_id = session[:grammartest_id]
    user_grammartest.grade = @result
    user_grammartest.save!
    destroy_session
  end

  private
    def destroy_session
      session.delete(:grammartest_id)
      session.delete(:question_grammar_id)
      session.delete(:question_answers_grammar)

      session.delete(:count_question)
      session.delete(:question_total)
      session.delete(:question_timer)
      session.delete(:test)
      @@grammars = nil
    end
end
