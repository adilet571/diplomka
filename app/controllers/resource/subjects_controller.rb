class Resource::SubjectsController < ApplicationController
  def index
    @subject = Subject.find(params[:id])
    @resources = @subject.subject_resources
  end
end
