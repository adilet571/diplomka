class CommentsController < ApplicationController
  before_action :authenticate_user!, only: [:new,:create,:destroy]

  def create
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @comment = @question.comments.new(comment_params)
    @comment.user_id = current_user.id
    @comments = @question.comments
    to = @question.user

    if @comment.save
      #UserNotifier.welcome_message(current_user).deliver
      ModelMailer.new_record_notification(current_user, @comment, to, request.url.chomp("comments")).deliver_later
      respond_to do |format|
        format.html do
          redirect_to [@forum, @question]
        end
        format.js
      end
    end
  end

  def show
  end

  def edit
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @comment = Comment.find(params[:id])
  end

  def update
    @forum = Forum.find(params[:forum_id])
    @comment = Comment.find(params[:id])
    @question = Question.find(params[:question_id])
    @comment.update_attributes(comment_params)

    @comments = @question.comments
  end

  def new
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @comment = @question.comments.new
  end

  def delete
  end

  def destroy
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:question_id])
    @comments = @question.comments
    @comment = Comment.find(params[:id])

    @comment.destroy
  end

  private
  def comment_params
    params.require(:comment).permit(:content)
  end
end
