class ProfilesController < ApplicationController
  before_filter :authenticate_user!
  before_action :get_ort_max_result

  def index
    @users_test_datas = []
    @users = [""]
    @grades = [0]

    @orts = GradeOrt.where(user_id: current_user.id).select(:ort_id).map(&:ort_id).uniq

    if params[:id].nil?
      @ort = Ort.first
    else
      @ort = Ort.find(params[:id])
    end

    @ort.grade_orts.select(:user_id).uniq.each do |test|
      @users_test_datas << test.user.id
    end

    @users << current_user.name
    @grades << current_user.grade_orts.where(ort_id: @ort.id).maximum("result")

    current_user.friendships.select(:friend_id).uniq.each do |friend|
      if @users_test_datas.include?(friend.friend.id)
        @grades << friend.friend.grade_orts.where(ort_id: @ort.id).maximum("result")
        @users << friend.friend.name
      end
    end

    @users << "Жалпы Суроо"
    @grades << @ort.analogies.count + @ort.kolonkas.count + @ort.algebras.count + @ort.readings.count + @ort.grammars.count
  end

  def math
    @users_mathtests = []
    @users = [""]
    @grades = [0]

    @mathtests = UserMathtest.where(user_id: current_user.id).select(:mathtest_id).map(&:mathtest_id).uniq

    if params[:id].nil?
      @mathtest = Mathtest.first
    else
      @mathtest = Mathtest.find(params[:id])
    end

    @mathtest.user_mathtests.each do |test|
      @users_mathtests << test.user.id
    end

    @users << current_user.name
    @grades << current_user.user_mathtests.where(mathtest_id: @mathtest.id).maximum("grade")

    current_user.friendships.select(:friend_id).uniq.each do |friend|
      if @users_mathtests.include?(friend.friend.id)
        @grades << friend.friend.user_mathtests.where(mathtest_id: @mathtest.id).maximum("grade")
        @users << friend.friend.name
      end
    end

    @users << "Жалпы Суроо"
    @grades << @mathtest.mathtest_part_ones.count + @mathtest.mathtest_part_twos.count
  end

  def grammar
    @users_grammartests = []
    @users = [""]
    @grades = [0]

    @grammartests = UserGrammartest.where(user_id: current_user.id).select(:grammartest_id).map(&:grammartest_id).uniq

    if params[:id].nil?
      @grammartest = Grammartest.first
    else
      @grammartest = Grammartest.find(params[:id])
    end

    @grammartest.user_grammartests.each do |test|
      @users_grammartests << test.user.id
    end

    @users << current_user.name
    @grades << current_user.user_grammartests.where(grammartest_id: @grammartest.id).maximum("grade")

    current_user.friendships.select(:friend_id).uniq.each do |friend|
      if @users_grammartests.include?(friend.friend.id)
        @grades << friend.friend.user_grammartests.where(grammartest_id: @grammartest.id).maximum("grade")
        @users << friend.friend.name
      end
    end

    @users << "Жалпы Суроо"
    @grades << @grammartest.grammartest_questions.count
  end

  def reading
    @users_readingtests = []
    @users = [""]
    @grades = [0]

    @readingtests = UserReadingtest.where(user_id: current_user.id).select(:readingtest_id).map(&:readingtest_id).uniq

    if params[:id].nil?
      @readingtest = Readingtest.first
    else
      @readingtest = Readingtest.find(params[:id])
    end

    @readingtest.user_readingtests.each do |test|
      @users_readingtests << test.user.id
    end

    @users << current_user.name
    @grades << current_user.user_readingtests.where(readingtest_id: @readingtest.id).maximum("grade")

    current_user.friendships.select(:friend_id).uniq.each do |friend|
      if @users_readingtests.include?(friend.friend.id)
        @grades << friend.friend.user_readingtests.where(readingtest_id: @readingtest.id).maximum("grade")
        @users << friend.friend.name
      end
    end

    @users << "Жалпы Суроо"
    @grades << @readingtest.readingtest_questions.count
  end

  def analogy
    @users_analogytests = []
    @users = [""]
    @grades = [0]

    @analogytests = UserAnalogytest.where(user_id: current_user.id).select(:analogytest_id).map(&:analogytest_id).uniq

    if params[:id].nil?
      @analogytest = Analogytest.first
    else
      @analogytest = Analogytest.find(params[:id])
    end

    @analogytest.user_analogytests.each do |test|
      @users_analogytests << test.user.id
    end

    @users << current_user.name
    @grades << current_user.user_analogytests.where(analogytest_id: @analogytest.id).maximum("grade")

    current_user.friendships.select(:friend_id).uniq.each do |friend|
      if @users_analogytests.include?(friend.friend.id)
        @grades << friend.friend.user_analogytests.where(analogytest_id: @analogytest.id).maximum("grade")
        @users << friend.friend.name
      end
    end

    @users << "Жалпы Суроо"
    @grades << @analogytest.analogytest_questions.count
  end

  private
  def get_ort_max_result
    @best_ort_score = current_user.grade_orts.maximum(:result)
    @best_ort_score_date = current_user.grade_orts.where(result: @best_ort_score).first
  end
end
