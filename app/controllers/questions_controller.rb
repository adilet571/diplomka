class QuestionsController < ApplicationController

  before_filter :authenticate_user!, only: [:new,:vote_question_up,:vote_question_down]
  after_filter :store_location

  def index
    if params[:tags]
      @forum = Forum.find(params[:forum_id])
      @questions = @forum.questions.tagged_with(params[:tags])
      @populars = @forum.questions.tagged_with(params[:tags])
    else
      @forum = Forum.find(params[:forum_id])
      @questions = @forum.questions.order('created_at desc')
      @populars = @forum.questions.order('view desc')
    end

    @tags = Tag.all
  end

  def vote_question_up

    @question = Question.find(params[:format])
    vote = @question.vote
    user = @question.user

    if vote.nil? && user != current_user
      vote = @question.build_vote
      vote.users << current_user
      @question.grade += 1

      if @question.save!
        vote.save!
      end
    elsif !vote.nil? && !vote.users.exists?(current_user) && user != current_user
      vote.users << current_user
      @question.grade += 1

      if @question.save!
        vote.save!
      end
    elsif user == current_user
      @message = 'You cannot vote up your question!'
    end
  end

  def vote_question_down

    @question = Question.find(params[:format])
    user = @question.user
    vote = @question.vote

    if vote.nil? && user != current_user
      vote = @question.build_vote
      vote.users << current_user

      @question.grade -= 1

      if @question.save!
        vote.save!
      end
    elsif !vote.nil? && !vote.users.exists?(current_user) && user != current_user
      vote.users << current_user
      @question.grade -= 1

      if @question.save!
        vote.save!
      end
    elsif user == current_user
      @message = 'You cannot vote down your question!'
    end

  end

  def show

    @question = Question.find(params[:id])
    @forum = Forum.find(params[:forum_id])

    if user_signed_in? && @question.user_id != current_user.id
      count_view
    elsif current_user.nil?
      count_view
    end

    @answers = @question.answers
    @comments = @question.comments

    @answer = @question.answers.new
    @comment = @question.comments.new


    @tags = @question.tags


  end

  def create
    forum = Forum.find(params[:forum_id])
    question = forum.questions.new(question_params)
    question.user_id = current_user.id
    question.view = 1

    if question.save
      redirect_to forum_question_url(forum,question), notice: 'Successfully Asked Question'
    else
      redirect_to new_forum_question_url(forum)
    end

  end

  def new
    @forum = Forum.find(params[:forum_id])
    @question = @forum.questions.new
  end

  def edit
    @forum = Forum.find(params[:forum_id])
    @question = Question.find(params[:id])
  end

  private
  def question_params
    params.require(:question).permit(:title,:content,:all_tags)
  end

  def count_view
    count = @question.view + 1
    @question.update_attribute(:view,count)
  end
end
