class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    # You need to implement the method below in your model (e.g. app/models/user.rb)


    auth = request.env["omniauth.auth"]
    token = auth['credentials']['token']

    user_friends = FbGraph2::User.me(token).fetch.friends
    contact = FbGraph2::User.me(token).fetch(fields: [:email, :first_name, :last_name])


    @user = User.from_omniauth(auth, contact, user_friends)



    puts 'start put friend id'
    user_friends.each do |friend|
      u = User.where(uid: friend.id).first
      if !u.nil? && !@user.friendships.exists?(u)
        @user.friendships.create(:friend_id => u.id)
      end
      puts u
      puts friend.id
      puts friend.name
    end
    puts 'end'



    session[:fb_access_token] = token
    session[:fb_user_uid] = auth['uid']

    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to root_path
    end
  end

  def google_oauth2
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth_google(request.env["omniauth.auth"])
    auth = request.env["omniauth.auth"]
    token = auth['credentials']['token']
    session[:fb_access_token] = token
    session[:fb_user_uid] = auth['uid']



    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Google") if is_navigational_format?
    else
      session["devise.google_data"] = request.env["omniauth.auth"]
      redirect_to root_path
    end
  end

  def failure
    redirect_to root_path
  end
end