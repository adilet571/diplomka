Rails.application.routes.draw do

  namespace :resource do
    resources :subjects
  end

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)
  get 'set_language/kyrgyz'

  get 'set_language/russian'

  get 'home/index'

  resources :forums, only: [:index, :new] do
    resources :questions do
      collection do
        post :vote_question_up
        post :vote_question_down
      end
      resources :answers,:comments do
        collection do
          post :vote_answer_up
          post :vote_answer_down
        end
        resources :answer_comments
      end
    end
  end



  resources :orts
  resources :profiles do
    collection do
      get :math
      get :analogy
      get :grammar
      get :reading
    end
  end

  namespace :ort do
    resources :infos

    resources :mains do
      collection do
        get :kolonka
        get :algebra
        get :analogy
        get :reading
        get :grammar
        get :email_ort
        post :check_reading
        post :check_kolonka
        post :check_algebra
        post :check_analogy
        post :check_grammar
        get :previous_grammar
        get :previous_reading
        get :previous_analogy
        get :previous_kolonka
        get :previous_algebra
        get :result
        get :start_over
      end
    end

    resources :grammar_quizzes do
      collection do
        post :check_grammar
        get :result
        get :previous_grammar
        get :start_over
      end
    end

    resources :reading_quizzes do
      collection do
        post :check_reading
        get :result
        get :previous_reading
        get :start_over
      end
    end

    resources :analogy_quizzes do
      collection do
        post :check_analogy
        get :result
        get :previous_analogy
        get :start_over
      end
    end

    resources :mathematic_quizzes do
      collection do
        post :check_part_one
        post :check_part_two
        get :result
        get :start_over
        get :home_page
        get :part_one
        get :part_two
        get :previous_part_one
        get :previous_part_two
      end
    end
  end

  devise_for :users, :controllers =>  { :omniauth_callbacks => "users/omniauth_callbacks" } do
    root 'admin'
  end

  as :user do
    get 'users/profile', :to => 'devise/registrations#edit', :as => :user_root
  end

  root to: 'home#index'
end
