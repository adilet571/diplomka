# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160608041318) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "algebras", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.text     "a",          limit: 65535
    t.text     "b",          limit: 65535
    t.text     "c",          limit: 65535
    t.text     "d",          limit: 65535
    t.integer  "ort_id",     limit: 4
    t.text     "definition", limit: 65535
    t.string   "answer",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.text     "e",          limit: 65535
  end

  add_index "algebras", ["ort_id"], name: "index_algebras_on_ort_id", using: :btree

  create_table "analogies", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.text     "a",          limit: 65535
    t.text     "b",          limit: 65535
    t.text     "c",          limit: 65535
    t.text     "d",          limit: 65535
    t.integer  "ort_id",     limit: 4
    t.text     "definition", limit: 65535
    t.string   "answer",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "analogies", ["ort_id"], name: "index_analogies_on_ort_id", using: :btree

  create_table "analogy_resources", force: :cascade do |t|
    t.integer  "language_id", limit: 4
    t.string   "video_url",   limit: 255
    t.string   "name",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "analogy_resources", ["language_id"], name: "index_analogy_resources_on_language_id", using: :btree

  create_table "analogytest_questions", force: :cascade do |t|
    t.integer  "analogytest_id", limit: 4
    t.text     "content",        limit: 65535
    t.text     "a",              limit: 65535
    t.text     "b",              limit: 65535
    t.text     "c",              limit: 65535
    t.text     "d",              limit: 65535
    t.string   "answer",         limit: 255
    t.text     "definition",     limit: 65535
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "analogytest_questions", ["analogytest_id"], name: "index_analogytest_questions_on_analogytest_id", using: :btree

  create_table "analogytests", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "locale",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "timer",      limit: 4
  end

  create_table "answer_comments", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.integer  "answer_id",  limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "answer_comments", ["answer_id"], name: "index_answer_comments_on_answer_id", using: :btree
  add_index "answer_comments", ["user_id"], name: "index_answer_comments_on_user_id", using: :btree

  create_table "answers", force: :cascade do |t|
    t.text     "content",     limit: 65535
    t.integer  "question_id", limit: 4
    t.integer  "user_id",     limit: 4
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "grade",       limit: 4,     default: 0
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree
  add_index "answers", ["user_id"], name: "index_answers_on_user_id", using: :btree

  create_table "articles", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.text     "content",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "articletests", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.text     "content",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: :cascade do |t|
    t.string   "content",     limit: 255
    t.integer  "question_id", limit: 4
    t.integer  "answer_id",   limit: 4
    t.integer  "user_id",     limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "comments", ["answer_id"], name: "index_comments_on_answer_id", using: :btree
  add_index "comments", ["question_id"], name: "index_comments_on_question_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "forums", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "title",              limit: 255
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
  end

  create_table "friendships", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "friend_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "friendships", ["friend_id"], name: "index_friendships_on_friend_id", using: :btree
  add_index "friendships", ["user_id"], name: "index_friendships_on_user_id", using: :btree

  create_table "grade_orts", force: :cascade do |t|
    t.decimal  "result",               precision: 10
    t.integer  "ort_id",     limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "grade_orts", ["ort_id"], name: "index_grade_orts_on_ort_id", using: :btree
  add_index "grade_orts", ["user_id"], name: "index_grade_orts_on_user_id", using: :btree

  create_table "grammar_resources", force: :cascade do |t|
    t.integer  "language_id", limit: 4
    t.string   "video_url",   limit: 255
    t.string   "name",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "grammar_resources", ["language_id"], name: "index_grammar_resources_on_language_id", using: :btree

  create_table "grammars", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.text     "a",          limit: 65535
    t.text     "b",          limit: 65535
    t.text     "c",          limit: 65535
    t.text     "d",          limit: 65535
    t.integer  "ort_id",     limit: 4
    t.text     "definition", limit: 65535
    t.string   "answer",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "grammars", ["ort_id"], name: "index_grammars_on_ort_id", using: :btree

  create_table "grammartest_questions", force: :cascade do |t|
    t.integer  "grammartest_id", limit: 4
    t.text     "content",        limit: 65535
    t.text     "a",              limit: 65535
    t.text     "b",              limit: 65535
    t.text     "c",              limit: 65535
    t.text     "d",              limit: 65535
    t.string   "answer",         limit: 255
    t.text     "definition",     limit: 65535
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "grammartest_questions", ["grammartest_id"], name: "index_grammartest_questions_on_grammartest_id", using: :btree

  create_table "grammartests", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "locale",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "timer",      limit: 4
  end

  create_table "kolonkas", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.string   "answer",     limit: 255
    t.text     "definition", limit: 65535
    t.integer  "ort_id",     limit: 4
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "a",          limit: 255,   default: "А"
    t.string   "b",          limit: 255,   default: "Б"
    t.string   "c",          limit: 255,   default: "В"
    t.string   "d",          limit: 255,   default: "Г"
  end

  add_index "kolonkas", ["ort_id"], name: "index_kolonkas_on_ort_id", using: :btree

  create_table "languages", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "locale",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "mathematic_resources", force: :cascade do |t|
    t.string   "video_url",     limit: 255
    t.string   "name",          limit: 255
    t.integer  "mathematic_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "mathematic_resources", ["mathematic_id"], name: "index_mathematic_resources_on_mathematic_id", using: :btree

  create_table "mathematics", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "locale",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "mathtest_part_ones", force: :cascade do |t|
    t.integer  "mathtest_id", limit: 4
    t.text     "content",     limit: 65535
    t.string   "answer",      limit: 255
    t.text     "definition",  limit: 65535
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "a",           limit: 255,   default: "А"
    t.string   "b",           limit: 255,   default: "Б"
    t.string   "c",           limit: 255,   default: "В"
    t.string   "d",           limit: 255,   default: "Г"
  end

  add_index "mathtest_part_ones", ["mathtest_id"], name: "index_mathtest_part_ones_on_mathtest_id", using: :btree

  create_table "mathtest_part_twos", force: :cascade do |t|
    t.integer  "mathtest_id", limit: 4
    t.text     "content",     limit: 65535
    t.text     "a",           limit: 65535
    t.text     "b",           limit: 65535
    t.text     "c",           limit: 65535
    t.text     "d",           limit: 65535
    t.text     "e",           limit: 65535
    t.string   "answer",      limit: 255
    t.text     "definition",  limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "mathtest_part_twos", ["mathtest_id"], name: "index_mathtest_part_twos_on_mathtest_id", using: :btree

  create_table "mathtests", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "locale",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "timer",      limit: 4
  end

  create_table "orts", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "locale",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "timer",      limit: 4
  end

  create_table "questions", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "content",    limit: 65535
    t.integer  "view",       limit: 4,     default: 0
    t.integer  "user_id",    limit: 4
    t.integer  "forum_id",   limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "grade",      limit: 4,     default: 0
  end

  add_index "questions", ["forum_id"], name: "index_questions_on_forum_id", using: :btree
  add_index "questions", ["user_id"], name: "index_questions_on_user_id", using: :btree

  create_table "questions_tags", id: false, force: :cascade do |t|
    t.integer "tag_id",      limit: 4
    t.integer "question_id", limit: 4
  end

  add_index "questions_tags", ["question_id"], name: "index_questions_tags_on_question_id", using: :btree
  add_index "questions_tags", ["tag_id"], name: "index_questions_tags_on_tag_id", using: :btree

  create_table "reading_resources", force: :cascade do |t|
    t.integer  "language_id", limit: 4
    t.string   "video_url",   limit: 255
    t.string   "name",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "reading_resources", ["language_id"], name: "index_reading_resources_on_language_id", using: :btree

  create_table "readings", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.text     "a",          limit: 65535
    t.text     "b",          limit: 65535
    t.text     "c",          limit: 65535
    t.text     "d",          limit: 65535
    t.integer  "ort_id",     limit: 4
    t.integer  "article_id", limit: 4
    t.text     "definition", limit: 65535
    t.string   "answer",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "readings", ["article_id"], name: "index_readings_on_article_id", using: :btree
  add_index "readings", ["ort_id"], name: "index_readings_on_ort_id", using: :btree

  create_table "readingtest_questions", force: :cascade do |t|
    t.integer  "articletest_id", limit: 4
    t.integer  "readingtest_id", limit: 4
    t.text     "content",        limit: 65535
    t.text     "a",              limit: 65535
    t.text     "b",              limit: 65535
    t.text     "c",              limit: 65535
    t.text     "d",              limit: 65535
    t.string   "answer",         limit: 255
    t.text     "definition",     limit: 65535
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "readingtest_questions", ["articletest_id"], name: "index_readingtest_questions_on_articletest_id", using: :btree
  add_index "readingtest_questions", ["readingtest_id"], name: "index_readingtest_questions_on_readingtest_id", using: :btree

  create_table "readingtests", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "locale",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "timer",      limit: 4
  end

  create_table "subject_resources", force: :cascade do |t|
    t.integer  "subject_id", limit: 4
    t.string   "video_url",  limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "subject_resources", ["subject_id"], name: "index_subject_resources_on_subject_id", using: :btree

  create_table "subjects", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "locale",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "user_analogytests", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "analogytest_id", limit: 4
    t.integer  "grade",          limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "user_analogytests", ["analogytest_id"], name: "index_user_analogytests_on_analogytest_id", using: :btree
  add_index "user_analogytests", ["user_id"], name: "index_user_analogytests_on_user_id", using: :btree

  create_table "user_grammartests", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "grammartest_id", limit: 4
    t.integer  "grade",          limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "user_grammartests", ["grammartest_id"], name: "index_user_grammartests_on_grammartest_id", using: :btree
  add_index "user_grammartests", ["user_id"], name: "index_user_grammartests_on_user_id", using: :btree

  create_table "user_mathtests", force: :cascade do |t|
    t.integer  "mathtest_id", limit: 4
    t.integer  "user_id",     limit: 4
    t.decimal  "grade",                 precision: 10
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "user_mathtests", ["mathtest_id"], name: "index_user_mathtests_on_mathtest_id", using: :btree
  add_index "user_mathtests", ["user_id"], name: "index_user_mathtests_on_user_id", using: :btree

  create_table "user_readingtests", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "readingtest_id", limit: 4
    t.integer  "grade",          limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "user_readingtests", ["readingtest_id"], name: "index_user_readingtests_on_readingtest_id", using: :btree
  add_index "user_readingtests", ["user_id"], name: "index_user_readingtests_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "name",                   limit: 255
    t.string   "Locale",                 limit: 255
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.integer  "fb_user_uid",            limit: 4,   default: 0
    t.string   "fb_access_token",        limit: 255
    t.string   "pol",                    limit: 255
    t.string   "image",                  limit: 255
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vote_answers", force: :cascade do |t|
    t.integer  "answer_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "vote_answers", ["answer_id"], name: "index_vote_answers_on_answer_id", using: :btree

  create_table "vote_user_answers", force: :cascade do |t|
    t.integer  "vote_answer_id", limit: 4
    t.integer  "user_id",        limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "vote_user_answers", ["user_id"], name: "index_vote_user_answers_on_user_id", using: :btree
  add_index "vote_user_answers", ["vote_answer_id"], name: "index_vote_user_answers_on_vote_answer_id", using: :btree

  create_table "vote_users", force: :cascade do |t|
    t.integer  "vote_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "vote_users", ["user_id"], name: "index_vote_users_on_user_id", using: :btree
  add_index "vote_users", ["vote_id"], name: "index_vote_users_on_vote_id", using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "question_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "votes", ["question_id"], name: "index_votes_on_question_id", using: :btree

  add_foreign_key "algebras", "orts"
  add_foreign_key "analogies", "orts"
  add_foreign_key "analogy_resources", "languages"
  add_foreign_key "analogytest_questions", "analogytests"
  add_foreign_key "answer_comments", "answers"
  add_foreign_key "answer_comments", "users"
  add_foreign_key "answers", "questions"
  add_foreign_key "answers", "users"
  add_foreign_key "comments", "answers"
  add_foreign_key "comments", "questions"
  add_foreign_key "comments", "users"
  add_foreign_key "grade_orts", "orts"
  add_foreign_key "grade_orts", "users"
  add_foreign_key "grammar_resources", "languages"
  add_foreign_key "grammars", "orts"
  add_foreign_key "grammartest_questions", "grammartests"
  add_foreign_key "kolonkas", "orts"
  add_foreign_key "mathematic_resources", "mathematics"
  add_foreign_key "mathtest_part_ones", "mathtests"
  add_foreign_key "mathtest_part_twos", "mathtests"
  add_foreign_key "questions", "categories", column: "forum_id"
  add_foreign_key "questions", "users"
  add_foreign_key "reading_resources", "languages"
  add_foreign_key "readings", "articles"
  add_foreign_key "readings", "orts"
  add_foreign_key "readingtest_questions", "articletests"
  add_foreign_key "readingtest_questions", "readingtests"
  add_foreign_key "subject_resources", "subjects"
  add_foreign_key "user_analogytests", "analogytests"
  add_foreign_key "user_analogytests", "users"
  add_foreign_key "user_grammartests", "grammartests"
  add_foreign_key "user_grammartests", "users"
  add_foreign_key "user_mathtests", "mathtests"
  add_foreign_key "user_mathtests", "users"
  add_foreign_key "user_readingtests", "readingtests"
  add_foreign_key "user_readingtests", "users"
  add_foreign_key "vote_answers", "answers"
  add_foreign_key "vote_user_answers", "users"
  add_foreign_key "vote_user_answers", "vote_answers"
  add_foreign_key "vote_users", "users"
  add_foreign_key "vote_users", "votes"
  add_foreign_key "votes", "questions"
end
