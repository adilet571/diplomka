class CreateMathtests < ActiveRecord::Migration
  def change
    create_table :mathtests do |t|
      t.string :name
      t.string :locale

      t.timestamps null: false
    end
  end
end
