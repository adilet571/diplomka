class RemoveColumnFromMathtestPartOne < ActiveRecord::Migration
  def change
    remove_column :mathtest_part_ones, :a
    remove_column :mathtest_part_ones, :b
    remove_column :mathtest_part_ones, :c
    remove_column :mathtest_part_ones, :d

  end
end
