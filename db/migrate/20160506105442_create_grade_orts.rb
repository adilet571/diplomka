class CreateGradeOrts < ActiveRecord::Migration
  def change
    create_table :grade_orts do |t|
      t.decimal :result
      t.references :ort, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
