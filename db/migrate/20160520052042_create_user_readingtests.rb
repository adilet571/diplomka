class CreateUserReadingtests < ActiveRecord::Migration
  def change
    create_table :user_readingtests do |t|
      t.references :user, index: true, foreign_key: true
      t.references :readingtest, index: true, foreign_key: true
      t.integer :grade

      t.timestamps null: false
    end
  end
end
