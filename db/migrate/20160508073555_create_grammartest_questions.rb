class CreateGrammartestQuestions < ActiveRecord::Migration
  def change
    create_table :grammartest_questions do |t|
      t.references :grammartest, index: true, foreign_key: true
      t.text :content
      t.text :a
      t.text :b
      t.text :c
      t.text :d
      t.string :answer
      t.text :definition

      t.timestamps null: false
    end
  end
end
