class AddGradeToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :grade, :integer, default: 0
  end
end
