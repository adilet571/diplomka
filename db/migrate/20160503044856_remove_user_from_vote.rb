class RemoveUserFromVote < ActiveRecord::Migration
  def change
    remove_foreign_key :votes, :column => :user_id
    remove_column :votes,:user_id
  end
end
