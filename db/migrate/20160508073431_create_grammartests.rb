class CreateGrammartests < ActiveRecord::Migration
  def change
    create_table :grammartests do |t|
      t.string :name
      t.string :locale

      t.timestamps null: false
    end
  end
end
