class CreateOrts < ActiveRecord::Migration
  def change
    create_table :orts do |t|
      t.string :name
      t.string :locale

      t.timestamps null: false
    end
  end
end
