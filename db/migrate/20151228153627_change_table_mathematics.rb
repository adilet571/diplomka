class ChangeTableMathematics < ActiveRecord::Migration
  def self.up
    change_table :mathematics do |t|
      t.change :a, :text
      t.change :b, :text
      t.change :c, :text
      t.change :d, :text
      t.change :e, :text
    end
  end
end
