class CreateMathtestPartTwos < ActiveRecord::Migration
  def change
    create_table :mathtest_part_twos do |t|
      t.references :mathtest, index: true, foreign_key: true
      t.text :content
      t.text :a
      t.text :b
      t.text :c
      t.text :d
      t.text :e
      t.string :answer
      t.text :definition

      t.timestamps null: false
    end
  end
end
