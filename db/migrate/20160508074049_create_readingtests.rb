class CreateReadingtests < ActiveRecord::Migration
  def change
    create_table :readingtests do |t|
      t.string :name
      t.string :locale

      t.timestamps null: false
    end
  end
end
