class RenameVoteToGradeQuestion < ActiveRecord::Migration
  def change
    rename_column :questions, :vote, :grade
  end
end

