class CreateUserGrammartests < ActiveRecord::Migration
  def change
    create_table :user_grammartests do |t|
      t.references :user, index: true, foreign_key: true
      t.references :grammartest, index: true, foreign_key: true
      t.integer :grade

      t.timestamps null: false
    end
  end
end
