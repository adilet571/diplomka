class CreateVoteAnswers < ActiveRecord::Migration
  def change
    create_table :vote_answers do |t|
      t.references :answer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
