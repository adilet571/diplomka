class CreateAnalogytests < ActiveRecord::Migration
  def change
    create_table :analogytests do |t|
      t.string :name
      t.string :locale

      t.timestamps null: false
    end
  end
end
