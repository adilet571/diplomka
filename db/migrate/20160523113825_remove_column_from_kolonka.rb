class RemoveColumnFromKolonka < ActiveRecord::Migration
  def change
    remove_column :kolonkas, :a
    remove_column :kolonkas, :b
    remove_column :kolonkas, :c
    remove_column :kolonkas, :d
  end
end
