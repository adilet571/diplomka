class CreateUserAnalogytests < ActiveRecord::Migration
  def change
    create_table :user_analogytests do |t|
      t.references :user, index: true, foreign_key: true
      t.references :analogytest, index: true, foreign_key: true
      t.integer :grade

      t.timestamps null: false
    end
  end
end
