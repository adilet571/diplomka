class AddColumnToMathtestPartOne < ActiveRecord::Migration
  def change
    add_column :mathtest_part_ones,:a, :string, default: 'А'
    add_column :mathtest_part_ones,:b, :string, default: 'Б'
    add_column :mathtest_part_ones,:c, :string, default: 'В'
    add_column :mathtest_part_ones,:d, :string, default: 'Г'

  end
end
