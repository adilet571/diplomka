class AddColumnToKolonka < ActiveRecord::Migration
  def change
    add_column :kolonkas,:a, :string, default: 'А'
    add_column :kolonkas,:b, :string, default: 'Б'
    add_column :kolonkas,:c, :string, default: 'В'
    add_column :kolonkas,:d, :string, default: 'Г'
  end
end
