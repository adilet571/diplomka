class RemoveGradeFromQuestion < ActiveRecord::Migration
  def change
    remove_column :questions, :grade
  end
end
