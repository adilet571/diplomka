class AddTimerToMathtest < ActiveRecord::Migration
  def change
    add_column :mathtests, :timer, :integer
  end
end
