class CreateVoteUserAnswers < ActiveRecord::Migration
  def change
    create_table :vote_user_answers do |t|
      t.references :vote_answer, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
