class RenameAnswerVote < ActiveRecord::Migration
  def change
    rename_column :answers, :vote, :grade
  end
end
