class CreateKolonkas < ActiveRecord::Migration
  def change
    create_table :kolonkas do |t|
      t.text :content
      t.text :a
      t.text :b
      t.text :c
      t.text :d
      t.string :answer
      t.text :definition
      t.references :ort, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
