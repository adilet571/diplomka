class CreateUserMathtests < ActiveRecord::Migration
  def change
    create_table :user_mathtests do |t|
      t.references :mathtest, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.decimal :grade

      t.timestamps null: false
    end
  end
end
