class CreateReadingtestQuestions < ActiveRecord::Migration
  def change
    create_table :readingtest_questions do |t|
      t.references :articletest, index: true, foreign_key: true
      t.references :readingtest, index: true, foreign_key: true
      t.text :content
      t.text :a
      t.text :b
      t.text :c
      t.text :d
      t.string :answer
      t.text :definition

      t.timestamps null: false
    end
  end
end
