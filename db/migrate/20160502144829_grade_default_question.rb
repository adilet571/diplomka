class GradeDefaultQuestion < ActiveRecord::Migration
  def change
    change_column :questions, :grade, :integer, default: 0
  end
end
