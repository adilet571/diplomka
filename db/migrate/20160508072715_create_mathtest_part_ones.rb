class CreateMathtestPartOnes < ActiveRecord::Migration
  def change
    create_table :mathtest_part_ones do |t|
      t.references :mathtest, index: true, foreign_key: true
      t.text :content
      t.text :a
      t.text :b
      t.text :c
      t.text :d
      t.string :answer
      t.text :definition

      t.timestamps null: false
    end
  end
end
