class CreateSubjectResources < ActiveRecord::Migration
  def change
    create_table :subject_resources do |t|
      t.references :subject, index: true, foreign_key: true
      t.string :video_url
      t.string :name

      t.timestamps null: false
    end
  end
end
