class CreateAnalogies < ActiveRecord::Migration
  def change
    create_table :analogies do |t|
      t.text :content
      t.text :a
      t.text :b
      t.text :c
      t.text :d
      t.references :ort, index: true, foreign_key: true
      t.text :definition
      t.string :answer

      t.timestamps null: false
    end
  end
end
