class AddTimerToReadingtest < ActiveRecord::Migration
  def change
    add_column :readingtests, :timer, :integer
  end
end
