require 'test_helper'

class SetLanguageControllerTest < ActionController::TestCase
  test "should get kyrgyz" do
    get :kyrgyz
    assert_response :success
  end

  test "should get russian" do
    get :russian
    assert_response :success
  end

end
