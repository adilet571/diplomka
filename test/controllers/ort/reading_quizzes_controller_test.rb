require 'test_helper'

class Ort::ReadingQuizzesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get start_over" do
    get :start_over
    assert_response :success
  end

end
